#!/usr/bin/env python3

import os
import argparse
import xml.etree.ElementTree as ET

ns = {
    'x': 'adobe:ns:meta/',
    'rdf': 'http://www.w3.org/1999/02/22-rdf-syntax-ns#',
    'xmp': 'http://ns.adobe.com/xap/1.0/',
    'xmpMM': 'http://ns.adobe.com/xap/1.0/mm/',
    'darktable': 'http://darktable.sf.net/',
}

for key, uri in ns.items():
    ET.register_namespace(key, uri)

class XMP:
    def __init__(self, path):
        self.file_ = open(path, "r+")
        self._parse()

    def _parse(self):
        self.tree = ET.ElementTree(file=self.file_)
        self.root = self.tree.getroot()

    def write(self):
        self.file_.seek(0)
        self.file_.truncate()

        self.tree.write(self.file_, xml_declaration=True, encoding="unicode")

    @property
    def rating(self):
        desc = self.root.find("./rdf:RDF/rdf:Description", ns)
        rating = desc.get("{%s}Rating" % ns["xmp"])
        return rating

    @rating.setter
    def rating(self, val):
        desc = self.root.find("./rdf:RDF/rdf:Description", ns)
        rating = desc.set("{%s}Rating" % ns["xmp"], val)



def merge(src, dest):
    for root, dirs, files in os.walk(src):
        subpath = root[len(src):]
        if len(files) == 0:
            # no files in dir, skipping...
            continue

        for f in files:
            if not f.endswith(".xmp"):
                continue

            src_path = os.path.join(root, f)
            dest_path = os.path.join(dest, subpath, f)
            if os.path.isfile(dest_path):
                print("overwriting rating from {} to {}".format(src_path, dest_path))
                # todo: merge times
                s = XMP(src_path)
                d = XMP(dest_path)
                print("{} to {}".format(d.rating, s.rating))
                d.rating = s.rating
                d.write()
            else:
                raise ValueError("Sidecar missing in destination path. Please import in darktable to generate a new, empty one")


def main():
    parser = argparse.ArgumentParser(
            description="Copy Rating from darktable xmp sidecars in FROM to corresponding sidecars in TO. Both paths have to end with a trailing /, because of lazieness.")
    parser.add_argument("FROM", help="Source Directory")
    parser.add_argument("TO", help="Destination Directory")

    args = parser.parse_args()
    for arg in [args.FROM, args.TO]:
        if not os.path.isdir(arg):
            raise ValueError("FROM and TWO have to be existing directories")
        if not arg.endswith("/"):
            raise ValueError("FROM and TWO need to have trailing slashes")

    merge(args.FROM, args.TO)

if __name__ == "__main__":
    main()
